<?
define('IS_DEBUG_FILE', __DIR__ . '/../debug.txt');

function isDebug() {
//    return true;
    return false;
    return isset($_COOKIE['is-debug-3985']) and $_COOKIE['is-debug-3985'] == '2352536';
}

function preTrace() {
    if (!isDebug())
        return;
    $e = new Exception();
    ?><pre><?= $e->getTraceAsString() ?></pre><?
}

function preTime() {
    if (!isDebug())
        return;
    if (func_num_args()) {
        pre(func_get_args());
    }
    pre(date('d.m.Y H:i:s') . ' ' . (memory_get_usage(true) / 1024 / 2014));
}

function pre() {
    if (!isDebug())
        return;
    echo '<pre>';
    foreach (func_get_args() as $value)
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    echo '</pre>';
//    preTrace();
}

function preDebugStart() {
    file_put_contents(IS_DEBUG_FILE, PHP_EOL . date('d.m.Y H:i:s'), FILE_APPEND);
}

if (defined('IS_DEBUG')) {
    preDebugStart();
}

global $preDebug;
$preDebug = '';
register_shutdown_function(function() {
    global $preDebug;
    if (!empty($preDebug)) {
        global $preDebug;
        file_put_contents(IS_DEBUG_FILE, date('d.m.Y H:i:s') . PHP_EOL . $preDebug . PHP_EOL . file_get_contents(IS_DEBUG_FILE));
    }
});

function preDebug() {
//    if (!isDebug())
//        return;
    ob_start();
    echo '<pre>';
    foreach (func_get_args() as $value)
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    echo '</pre>';
//    file_put_contents(IS_DEBUG_FILE, PHP_EOL . ob_get_clean(), FILE_APPEND);
    global $preDebug;
    $preDebug = ob_get_clean();
}

function preExit() {
    if (!isDebug())
        return;
    echo '<pre>';
    foreach (func_get_args() as $value)
        if (is_array($value) or is_object($value)) {
            print_r($value);
        } else {
            var_dump($value);
        }
    echo '</pre>';
    exit;
}

function preMemory() {
    pre('memory: ' . round(memory_get_usage() / 1024 / 1024, 3));
}

function preHtml() {
    if (!isDebug())
        return;
//    pre(func_get_args());
//    return;
    echo '<pre>';
    foreach (func_get_args() as $value) {
        $value = preg_replace('~(</[^>]+>)~', '$1' . PHP_EOL, $value);
        $value = str_replace('/><', '/>' . PHP_EOL . '<', $value);
        echo htmlspecialchars($value);
    }
    echo '</pre>';
}