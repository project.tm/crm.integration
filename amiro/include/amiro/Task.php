<?php

namespace Local\Amiro;

class Task {

    static public function add($contactId, $arData) {
        $fields = Utility::getFields('task');
        $DESCRIPTION = array();
        foreach ($arData as $key => $value) {
            $DESCRIPTION[] = $key . ': ' . $value;
        }

        $task = array();
        $task['request']['tasks']['add'] = array(
            array(
                'element_id' => $contactId,
                'element_type' => 1,
                'task_type' => 2, #Встреча
                'text' => implode(PHP_EOL, $DESCRIPTION),
                'responsible_user_id' => Config::RESPONSIBLE_ID,
                'complete_till' => time() + 12 * 60 * 60,
            ),
        );
        pre($task);
        $response = Utility::curl('v2/json/tasks/set', $task);
    }

}
