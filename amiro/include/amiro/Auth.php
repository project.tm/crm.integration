<?php

namespace Local\Amiro;

class Auth {

    static public function init() {
        $user = array(
            'USER_LOGIN' => Config::USER_LOGIN, #Ваш логин (электронная почта)
            'USER_HASH' => Config::USER_HASH #Хэш для доступа к API (смотрите в профиле пользователя)
        );
        $response = Utility::curl('auth.php?type=json', $user);
        return isset($response['response']['auth']);
    }

}
