<?php

namespace Local\Amiro;

class Lead {

    static public function add($arData) {
        $fields = Utility::getFields('task');
        $description = array();
        foreach ($arData as $key => $value) {
            if($value) {
                $description[] = $key . ': ' . $value;
            }
        }
        $description = implode(PHP_EOL, $description);

        $task = array();
        $task['request']['leads']['add'] = array(
            array(
                'name' => $arData['Сайт'] . (empty($arData['Имя']) ? '' : ' ' . $arData['Имя']) . ': ' . $arData['Телефон'],
                'date_create' => time(),
//                'status_id'=>'15172590',
                'custom_fields' => Utility::setFields(array(
                    163599 => $description,
                    163607 => $arData['Телефон'],
                    163613 => $arData['Email'],
                    163615 => $arData['Сайт'],
                    163617 => $arData['roistat'],
                        )),
                "tags" => $arData['Сайт'],
//                'element_id' => $contactId,
//                'element_type' => 1,
//                'task_type' => 2, #Встреча
//                'text' => implode(PHP_EOL, $DESCRIPTION),
                'responsible_user_id' => Config::RESPONSIBLE_ID,
//                'complete_till' => time() + 12 * 60 * 60,
            ),
        );
        $response = Utility::curl('v2/json/leads/set', $task);
        if ($response and isset($response['response']['leads']['add'][0]['id'])) {
            return $response['response']['leads']['add'][0]['id'];
        } else {
            throw new \Exception('Невозможно создать лид');
        }
    }

}
