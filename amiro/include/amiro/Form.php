<?php

namespace Local\Amiro;

class Form {

    static public function send($arData) {
        try {
//            pre($arData);
            if (!Auth::init()) {
                return;
            }
            $leadId = Lead::add($arData);
            $contactId = Contact::search($arData, $leadId);
//            Task::add($contactId, $arData);
        } catch (\Exception $exc) {
            pre($exc->getTraceAsString());
            pre('Ошибка: ' . $exc->getMessage() . PHP_EOL . 'Код ошибки: ' . $exc->getCode());
        }
    }

}
