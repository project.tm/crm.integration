<?php

namespace Local\Amiro;

class Utility {

    static private $fields = array();

    static private function getFile($file) {
        return __DIR__ . '/data/' . $file . '.txt';
    }

    static public function setFields($param) {
        $arResult = array();
        foreach ($param as $key => $value) {
            $arResult[] = array(
                'id' => $key,
                'values' => array(
                    array(
                        'value' => $value,
                    )
                )
            );
        }
        return $arResult;
    }

    static public function getFields($param) {
        if (empty(self::$fields)) {
            $response = Utility::curl('v2/json/accounts/current');
            $account = $response['response']['account'];
            foreach (array(
        'contacts' => array('POSITION', 'PHONE', 'EMAIL', 'IM')
            ) as $key => $need) {

                $need = array_flip($need);
                if (isset($account['custom_fields'], $account['custom_fields'][$key])) {
                    do {
                        foreach ($account['custom_fields']['contacts'] as $field) {
                            if (is_array($field) && isset($field['id'])) {
                                if (isset($field['code']) && isset($need[$field['code']])) {
                                    $fields[$field['code']] = (int) $field['id'];
                                    #SCOPE - нестандартное поле, поэтому обрабатываем его отдельно
                                } elseif (isset($field['name']) && $field['name'] == 'Сфера деятельности') {
                                    $fields['SCOPE'] = $field;
                                }

                                $diff = array_diff_key($need, $fields);
                                if (empty($diff)) {
                                    break 2;
                                }
                            }
                        }
                        if (isset($diff)) {
                            throw new \Exception('В amoCRM отсутствуют следующие поля' . ': ' . join(', ', $diff));
                        } else {
                            throw new \Exception('Невозможно получить дополнительные поля');
                        }
                    } while (false);
                } else {
                    throw new \Exception('Невозможно получить дополнительные поля');
                }
                self::$fields[$key] = isset($fields) ? $fields : false;
            }
        }
        return isset(self::$fields[$param]) ? self::$fields[$param] : array();
    }

    static public function curl($func, $data = false, $isJson = true) {
#Формируем ссылку для запроса
        $link = 'https://' . Config::SUBDOMAIN . '.amocrm.ru/private/api/' . $func;
        pre($link);
        $curl = curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        if ($data) {
            curl_setopt($curl, CURLOPT_POST, true);
            if ($isJson) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            } else {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $isJson ?: http_build_query($data));
            }
        }
//        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_COOKIEFILE, self::getFile('cookie')); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, self::getFile('cookie')); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
        curl_close($curl); #Завершаем сеанс cURL
        self::checkResponse($code);
        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
//        pre(self::getFile('cookie'), file_get_contents(self::getFile('cookie')));
        return $isJson ? json_decode($out, true) : $out;
    }

    static public function checkResponse($code) {
        $code = (int) $code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
        if ($code != 200 && $code != 204)
            throw new \Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
    }

}
